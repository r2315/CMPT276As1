const FLOAT_PRECISION = 4

const table = document.getElementById("activityTable")
const WEIGHT_COLUMN = 2
const GRADE_COLUMN = 3
const PERCENTAGE_COLUMN = 4

let activityButtonPressed = document.getElementsByClassName("addActivity")
activityButtonPressed[0].addEventListener('click', addActivity)

let meanButtonPressed = document.getElementById("mean")
meanButtonPressed.addEventListener('click', calculateMean)

let weightButtonPressed = document.getElementById("weighted")
weightButtonPressed.addEventListener('click', calculateWeightedMean)

function addActivity (e) {
    var newRow = table.insertRow();
        activityName = newRow.insertCell(0);
        shortName = newRow.insertCell(1);
        weight = newRow.insertCell(WEIGHT_COLUMN);
        grade = newRow.insertCell(GRADE_COLUMN);
        percent = newRow.insertCell(PERCENTAGE_COLUMN);

    activityName.innerHTML = "Activity " + newRow.rowIndex;
    shortName.innerHTML = "A" + newRow.rowIndex;
    weight.innerHTML = "<input type='text' value='' name='weight'>"
    grade.innerHTML = "<input id='grade' onchange= 'doCalculations()' type='text' value='' name='grade'> / <input id='totalPossibleMarks' onchange= 'doCalculations()' type='text' value='' name='totalPossibleMarks'>"
    document.getElementById("grandTotal").innerHTML = ""
    doCalculations()
}

function doCalculations () {
    for (var i = 1, row; row = table.rows[i]; i++) {
        var grades = row.cells[GRADE_COLUMN].firstElementChild
            totalPossibleMarks = row.cells[GRADE_COLUMN].lastElementChild
            percent = row.cells[PERCENTAGE_COLUMN]
            rowResult = (grades.value / totalPossibleMarks.value) * 100
        percent.innerHTML = rowResult.toPrecision(FLOAT_PRECISION)

        if (isNaN(rowResult) || !isFinite(rowResult)) {
            rowResult = 0
            percent.innerHTML = rowResult.toPrecision(FLOAT_PRECISION) + " (Invalid/empty inputs = 0)"
        }
    }
}

function calculateMean () {
    var grandTotal = 0

    for (var i = 1, row; row = table.rows[i]; i++) {
        grandTotal += parseFloat(row.cells[PERCENTAGE_COLUMN].innerHTML)
    }
    document.getElementById("grandTotal").innerHTML = (grandTotal / (i - 1)).toPrecision(FLOAT_PRECISION) + '%'
}

function calculateWeightedMean () {
    var grandTotal = 0
        weightTotal = 0

    for (var i = 1, row; row = table.rows[i]; i++) {
        var activityWeight = parseFloat(row.cells[WEIGHT_COLUMN].firstElementChild.value)
        
        if (isNaN(activityWeight) || !isFinite(activityWeight)) {
            activityWeight = 0
        }
        grandTotal += parseFloat(row.cells[PERCENTAGE_COLUMN].innerHTML) * activityWeight
        weightTotal += activityWeight
    }
    document.getElementById("grandTotal").innerHTML = (grandTotal / weightTotal).toPrecision(FLOAT_PRECISION) + '%'
}